/*
Author: Seth Renshaw (srenshaw1@gmail.com)
Date Created: 2017-01-30
*/

#pragma once

#define ForEach(container)\
  for (auto it = (container).iterate(); !it; ++it)