/*
Author: Seth Renshaw (srenshaw1@gmail.com)
Date Created: 2017-01-28
*/

#define TFunc(type_name,return_type)\
template <typename type_name>\
return_type