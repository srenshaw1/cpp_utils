/*
File: 'sr_string.h'
Author: Seth Renshaw
Date Created: 7/27/2016
*/

#pragma once

#include <stdlib.h>
#include <stdint.h>

struct String {
  uint32_t   size;
  char* text;

  String(): text(nullptr), size(0) {}

  String(char const s []): String() {
    if (s) {
      for (size = 0; s[size] != '\0'; ++size);
      reset(s, size);
    }
  }

  String(String const& s): String() {
    reset(s.text, s.size);
  }

  String(uint64_t num): String() {
    parseInt(num);
  }

  String(uint32_t num): String((uint64_t)num) {}

  String(int64_t num): String() {
    parseInt(num);
  }

  String(int32_t num): String((int64_t)num) {}

  String(double num, uint32_t decimal = 2): String() {
    parseFloat(num, decimal);
  }

  String(float num, uint32_t decimal = 2): String((double)num, decimal) {}

  ~String() {
    if (text)
      free(text);
  }

  void parseInt(int64_t num, bool initializing = true) {
    bool negative = false;
    if (num < 0) {
      num = -num;
      negative = true;
    }

    int64_t n = num;
    uint32_t count;
    for (count = 0; n > 0; n /= 10)
      count++;
    if (negative)
      count++;

    char* reverse_num = (char*)malloc(sizeof(char) * count);
    count = 0;

    for (n = num; n > 0; n /= 10)
      reverse_num[count++] = n % 10 + '0';

    if (negative)
      reverse_num[count++] = '-';

    if (count > 0) {
      text = initializing
        ? (char*)malloc(sizeof(char) * (count+1))
        : (char*)realloc(text, sizeof(char) * (count+1));

      for (size = 0; count > 0; --count)
        text[size++] = reverse_num[count - 1];
    } else {
      size = 1;
      text = initializing
        ? (char*)malloc(sizeof(char) * (size+1))
        : (char*)realloc(text, sizeof(char) * (size+1));
      text[0] = '0';
    }
    text[size] = '\0';
    free(reverse_num);
  }

  void parseFloat(double num, uint32_t decimal, bool initializing = true) {
    char fmt [6];
    sprintf(fmt, "%% .%df", decimal);
    char str_num [128];
    sprintf(str_num, fmt, num);
    for (size = 0; str_num[size] != '\0'; ++size);
    reset(str_num, size, initializing);
  }

  void reset(char const str [], uint32_t str_size, bool initializing = true) {
    if (!str)
      return;

    if (initializing) {
      text = (char*)malloc(sizeof(char) * (str_size + 1));
      for (uint32_t i = 0; i < (str_size + 1); ++i)
        text[i] = str[i];
      size = str_size;
    } else {
      text = (char*)realloc(text, sizeof(char) * (size + str_size + 1));
      for (uint32_t i = size; i < (size + str_size + 1); ++i)
        text[i] = str[i-size];
      size += str_size;
    }
  }

  void clear() {
    if (text) {
      free(text);
      text = nullptr;
      size = 0;
    }
  }

  void append(uint64_t n) {
    String s;
    s.fromU64(n);
    append(s);
  }

  void append(String const& s) {
    reset(s.text, s.size, false);
    text[size] = '\0';
  }

  char pop() {
    if (size) {
      char c = text[size--];
      text[size] = '\0';
      return c;
    } else
      return '\0';
  }

  //TODO(seth): this needs to be checked for overrun
  uint64_t toU64() {
    uint64_t num = 0;
    for (uint32_t i = 0; i < size; ++i) {
      num *= 10;
      num += text[i] - '0';
    }
    return num;
  }

  void fromU64(uint64_t num) {
    parseInt(num, false);
  }

  friend
  String& operator<<(String& out, uint64_t n) {
    out.append(n);
    return out;
  }

  friend
  String& operator<<(String& out, String const& in) {
    out.append(in);
    return out;
  }

  inline
  String& operator=(String const& s) {
    clear();
    reset(s.text, s.size);
    return *this;
  }

  inline
  String operator+(String const& s) {
    String str(*this);
    str.append(s);
    return str;
  }

  inline
  bool operator==(String const& s) const {
    if (size != s.size)
      return false;

    for (uint32_t i = 0; i < size; ++i)
      if (text[i] != s.text[i])
        return false;

    return true;
  }
};

inline String
operator+(char const* c, String const& s) {
  String str(c);
  str.append(s);
  return str;
}

inline bool
operator==(char const* c, String const& s) {
  return s == c;
}
