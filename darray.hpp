#pragma once

#include "helper/dbg.h"
#include "helper/template.hpp"
#include "types.h"

internal u32 const DefaultExpandRate = 300;

template <typename Type>
struct DArray {
  struct Iterator {
    Iterator(DArray *da): array(da), index(0) {}

    b32   operator! (void) { return index < array->size; }
    void  operator++(void) { ++index; }
    Type *operator& (void) { return &array->contents[index]; }
    Type  operator* (void) { return array->contents[index]; }
    Type *operator->(void) { return &array->contents[index]; }

    u32     index;
    DArray *array;
  };

  DArray(u32 initial_max = 100, u32 exp_rate = DefaultExpandRate):
    max_value(initial_max ? initial_max : 1),
    size(0),
    element_size(sizeof(Type)),
    expand_rate(exp_rate),
    contents(new Type [initial_max])
  {}
  ~DArray() { delete [] contents; }

  b32  push_back(Type el);
  Type pop_back (void);
  void set      (u32 i, Type el);
  Type get      (u32 i);

  void operator()(u32 i, Type el) { set(i, el); }
  Type operator()(u32 i) { return get(i); }

  Iterator iterate() { return Iterator(this); }

  u32   size;
  u32   max_value;
  u32   element_size;
  u32   expand_rate;
  Type *contents;
};

template <typename T>
b32
DArray<T>::push_back(T el) {
  contents[size++] = el;
  return (size >= max_value) ? expand(this) : true;
}

template <typename T>
T
DArray<T>::pop_back() {
  check(size - 1 >= 0, "Attempt to pop from empty array.");
  //NOTE(seth): Skip this block if the check fails.
  {
    T el = remove(this, size - 1);
    size--;
    if (size > expand_rate && size % expand_rate)
      contract(this);

    return el;
  }
error:
  return T();
}

template <typename T>
void
DArray<T>::set(u32 i, T el) {
  check(i < max_value, "DArray attempt to set past max_value.");
  if (i >= size)
    size = i+1;
  contents[i] = el;
error:
  return;
}

template <typename T>
T
DArray<T>::get(u32 i) {
  check(i < max_value, "DArray attempt to get past max_value.");
  return contents[i];
error:
  return T();
}

template <typename T>
b32
resize(DArray<T> *array, u32 new_max) {
  check(new_max > 0, "The newsize must be > 0.");

  T *temp_contents = (T *)realloc(array->contents, new_max * sizeof(T));
  check_mem(temp_contents);

  array->max_value = new_max;
  array->contents = temp_contents;

  return true;

error:
  return false;
}

template <typename T>
b32
expand(DArray<T> *array) {
  u32 old_max = array->max_value;
  check(resize(array, array->max_value + array->expand_rate), "Failed to expand array to new size: %d", array->max_value + (u32)array->expand_rate);

  memset(array->contents + old_max, 0, array->expand_rate + 1);

  return true;

error:
  return false;
}

template <typename T>
b32
contract(DArray<T> *array) {
  return resize(array, ((array->size < array->expand_rate) ? array->expand_rate : array->size) + 1);
}

template <typename T>
T
remove(DArray<T> *array, u32 i) {
  T el = array->contents[i];
  array->contents[i] = {};
  return el;
}
