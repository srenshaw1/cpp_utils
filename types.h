/*
File: 'MekTool_CPP/types.h'
Author: Seth Renshaw
Date Created: 7/27/2016
*/

#ifndef TYPES_H
#define TYPES_H

#include <stdint.h>

#define internal static
#define local    static
#define global   static

#define u8  uint8_t
#define u16 uint16_t
#define u32 uint32_t
#define u64 uint64_t

#define s8  int8_t
#define s16 int16_t
#define s32 int32_t
#define s64 int64_t

#define r32 float
#define r64 double

#define b32 uint32_t

#endif