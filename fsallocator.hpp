
/*
Author  : Seth Renshaw (srenshaw1@gmail.com)
Date    : 2017-07-19 20:44:23
*/

#pragma once

template<typename Type, uint32_t N>
struct FixedSizeAllocator {
  struct Iterator {
    Iterator(FixedSizeAllocator *fsa): memory(fsa), index(0) {
      if (!memory->used[index])
        operator++();
    }

    inline void operator++(void) {
      while (index < memory->num_initialized && !memory->used[++index]);
    }

    inline b32   operator! (void) { return index < memory->num_initialized; }
    inline Type *operator& (void) { return (Type *)memory->toAddress(index); }
    inline Type &operator* (void) { return *(Type *)memory->toAddress(index); }
    inline Type *operator->(void) { return (Type *)memory->toAddress(index); }

    uint32_t index;
    FixedSizeAllocator *memory;
  };

  inline
  Iterator iterate() { return Iterator(this); }

  uint32_t block_size;
  uint32_t num_blocks;
  uint32_t num_initialized;
  uint32_t num_free_blocks;

  uint8_t *open;
  bool    *used;
  uint8_t *memory;

  FixedSizeAllocator()
  : block_size(sizeof(Type) < 4 ? 4 : sizeof(Type))
  , num_blocks(N)
  , num_initialized(0)
  , num_free_blocks(N) {
    memory = (uint8_t *)malloc(block_size * num_blocks);
    used = (bool *)malloc(num_blocks);
    open = memory;
  }

  ~FixedSizeAllocator() {
    free(memory);
    memory = nullptr;
  }

  inline
  Type *operator[](uint32_t index) { return (Type *)toAddress(index); }

  inline
  uint8_t *toAddress(uint32_t index) { return memory + index * block_size; }

  inline
  uint32_t toIndex(void *address) { return ((uint8_t *)address - memory) / block_size; }

  Type *alloc() {
    if (num_initialized < num_blocks) {
      uint32_t *next = (uint32_t *)toAddress(num_initialized);
      *next = num_initialized + 1;
      used[num_initialized] = false;
      ++num_initialized;
    }

    Type *block = nullptr;
    if (num_free_blocks > 0) {
      --num_free_blocks;
      block = (Type *)open;
      used[toIndex(open)] = true;
      open = toAddress(*(uint32_t *)open);
    } else {
      open = nullptr;
    }
    return block;
  }

  void dealloc(void *block) {
    if (block < memory || block > memory + block_size * num_blocks)
      return;
    used[toIndex(block)] = false;
    int index = toIndex(open);
    *(uint32_t *)block = open ? index : num_blocks;
    open = (uint8_t *)block;
    ++num_free_blocks;
  }
};
