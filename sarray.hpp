
/*
Author  : Seth Renshaw (srenshaw1@gmail.com)
Date    : 2017-08-27 14:44:59
Copyright: Seth Renshaw 2017-08-27 14:44:59
*/

#pragma once

template<typename Type, uint32_t N>
struct SArray {
  struct Iterator {
    Iterator(SArray *sa): array(sa), index(0) {}

    inline bool operator!(void) { return index < array->size; }
    inline void operator++(void) { ++index; }
    inline Type *operator&(void) { return &array->contents[index+1]; }
    inline Type &operator*(void) { return array->contents[index+1]; }
    inline Type *operator->(void) { return &array->contents[index+1]; }

  private:
    uint32_t index;
    SArray *array;
  };

  SArray(Type def = Type()): def_type(def), indices{}, size(0) {
    contents = (Type *)malloc(sizeof(Type));
    contents[0] = def_type;
  }

  ~SArray() {
    free(contents);
  }

  Type *operator()(uint32_t index, Type item) {
    if (index >= N)
      return nullptr;

    if (!indices[index])
      indices[index] = expand();

    contents[indices[index]] = item;
    return &contents[indices[index]];
  }

  Type *operator()(uint32_t index) {
    if (index >= N)
      return nullptr;

    return &contents[indices[index]];
  }

  inline bool contains(uint32_t index) { return indices[index]; }

  inline Iterator iterate() { return Iterator(this); }

  Type       def_type;
  uint32_t   indices[N];
  size_t     size;
  Type *contents;

private:
  size_t expand() {
    size += 1;
    contents = (Type *)realloc(contents, (size + 1) * sizeof(Type));
    return size;
  }
};