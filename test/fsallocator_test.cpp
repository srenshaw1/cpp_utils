
/*
Author  : Seth Renshaw (srenshaw1@gmail.com)
Date    : 2017-08-05 18:18:41
Copyright: Seth Renshaw 2017-08-05 18:18:41
*/

#include "../helper/minunit.h"

#include "../fsallocator.hpp"

namespace test_FSAllocator {
  FixedSizeAllocator<int32_t,3> fsa;

  char *test_alloc() {
    mu_assert(fsa.num_initialized == 0, "initialized allocator block number is incorrect.");
    mu_assert(fsa.num_free_blocks == 3, "free allocator block number is incorrect.");

    int32_t *ts = fsa.alloc();
    mu_assert(ts != nullptr, "non-full allocator returned NULL");
    mu_assert(fsa.used[0], "allocator block 0 is marked unused after allocation.");
    *ts = 1;

    mu_assert(fsa.num_initialized == 1, "initialized allocator block number is incorrect.");
    mu_assert(fsa.num_free_blocks == 2, "free allocator block number is incorrect.");

    ts = fsa.alloc();
    mu_assert(ts != nullptr, "non-full allocator returned NULL.");
    mu_assert(fsa.used[1], "allocator block 0 is marked unused after allocation.");
    *ts = 2;

    mu_assert(fsa.num_initialized == 2, "initialized allocator block number is incorrect.");
    mu_assert(fsa.num_free_blocks == 1, "free allocator block number is incorrect.");

    ts = fsa.alloc();
    mu_assert(ts != nullptr, "non-full allocator returned NULL.");
    mu_assert(fsa.used[2], "allocator block 0 is marked unused after allocation.");
    *ts = 3;

    mu_assert(fsa.num_initialized == 3, "initialized allocator block number is incorrect.");
    mu_assert(fsa.num_free_blocks == 0, "free allocator block number is incorrect.");

    ts = fsa.alloc();
    mu_assert(ts == nullptr, "full allocator returned non-NULL.");

    return nullptr;
  }

  char *test_dealloc() {
    fsa.dealloc(fsa.toAddress(0));
    mu_assert(fsa.num_free_blocks == 1, "free allocator block number is incorrect.");
    mu_assert(!fsa.used[0], "allocator block 0 is marked used after deallocation.");

    fsa.dealloc(fsa.toAddress(2));
    mu_assert(fsa.num_free_blocks == 2, "free allocator block number is incorrect.");
    mu_assert(!fsa.used[2], "allocator block 0 is marked used after deallocation.");

    return nullptr;
  }

  char *test_realloc() {
    FixedSizeAllocator<float, 100> all;

    for (int i = 0; i < 50; ++i) {
      float *value = all.alloc();
      *value = (i+1) * 10;
    }

    all.dealloc(all.toAddress( 0));
    all.dealloc(all.toAddress(10));
    all.dealloc(all.toAddress(20));
    all.dealloc(all.toAddress(30));
    all.dealloc(all.toAddress(40));

    for (int i = 0; i < 10; ++i) {
      float *value = all.alloc();
      *value = (i+10) * 30;
    }

    return nullptr;
  }

  char *test_index_operator() {
    mu_assert(*fsa[0] == 3, "allocator[0] is not 3.");
    mu_assert(*fsa[1] == 2, "allocator[1] is not 2.");
    mu_assert(*fsa[2] == 0, "allocator[2] is not 0.");

    return nullptr;
  }

  char *test_for_loop() {
    auto idx = 0;
    ForEach (fsa) {
      mu_assert(*it == 2, "iterator value is not correct.");
      ++idx;
    }

    mu_assert(idx == 1, "number of iterations is incorrect.");

    return nullptr;
  }

  int32_t nums[] = { 1, 2, 3 };
  FixedSizeAllocator<int32_t *,3> ptr_fsa;

  char *test_ptr_alloc() {
    mu_assert(ptr_fsa.num_initialized == 0, "initialized ptr_allocator block number is incorrect.");
    mu_assert(ptr_fsa.num_free_blocks == 3, "free ptr_allocator block number is incorrect.");

    int32_t **ts = ptr_fsa.alloc();
    mu_assert(ts != nullptr, "non-full ptr_allocator returned NULL");
    mu_assert(ptr_fsa.used[0], "ptr_allocator block 0 is marked unused after allocation.");
    *ts = &nums[0];

    mu_assert(ptr_fsa.num_initialized == 1, "initialized ptr_allocator block number is incorrect.");
    mu_assert(ptr_fsa.num_free_blocks == 2, "free ptr_allocator block number is incorrect.");

    ts = ptr_fsa.alloc();
    mu_assert(ts != nullptr, "non-full ptr_allocator returned NULL.");
    mu_assert(ptr_fsa.used[1], "ptr_allocator block 0 is marked unused after allocation.");
    *ts = &nums[1];

    mu_assert(ptr_fsa.num_initialized == 2, "initialized ptr_allocator block number is incorrect.");
    mu_assert(ptr_fsa.num_free_blocks == 1, "free ptr_allocator block number is incorrect.");

    ts = ptr_fsa.alloc();
    mu_assert(ts != nullptr, "non-full ptr_allocator returned NULL.");
    mu_assert(ptr_fsa.used[2], "ptr_allocator block 0 is marked unused after allocation.");
    *ts = &nums[2];

    mu_assert(ptr_fsa.num_initialized == 3, "initialized ptr_allocator block number is incorrect.");
    mu_assert(ptr_fsa.num_free_blocks == 0, "free ptr_allocator block number is incorrect.");

    ts = ptr_fsa.alloc();
    mu_assert(ts == nullptr, "full ptr_allocator returned non-NULL.");

    return nullptr;
  }

  char *test_ptr_dealloc() {
    ptr_fsa.dealloc(ptr_fsa.toAddress(0));
    mu_assert(ptr_fsa.num_free_blocks == 1, "free ptr_allocator block number is incorrect.");
    mu_assert(!ptr_fsa.used[0], "ptr_allocator block 0 is marked used after deallocation.");

    ptr_fsa.dealloc(ptr_fsa.toAddress(2));
    mu_assert(ptr_fsa.num_free_blocks == 2, "free ptr_allocator block number is incorrect.");
    mu_assert(!ptr_fsa.used[2], "ptr_allocator block 0 is marked used after deallocation.");

    return nullptr;
  }

  char *test_ptr_index_operator() {
    mu_assert(*(uint8_t *)ptr_fsa[0] == 3, "ptr_allocator[0] is not 3.");
    mu_assert(*ptr_fsa[1] == &nums[1], "ptr_allocator[1] is not 2.");
    mu_assert(*(uint8_t *)ptr_fsa[2] == 0, "ptr_allocator[2] is not 0.");

    return nullptr;
  }

  char *test_ptr_for_loop() {
    auto idx = 0;
    ForEach (ptr_fsa) {
      mu_assert(*it == &nums[1], "iterator value is not correct.");
      ++idx;
    }

    mu_assert(idx == 1, "number of iterations is incorrect.");

    return nullptr;
  }

  char *test_all() {
    mu_suite_start();

    mu_run_test(test_alloc);
    mu_run_test(test_dealloc);
    mu_run_test(test_realloc);
    mu_run_test(test_index_operator);
    mu_run_test(test_for_loop);

    mu_run_test(test_ptr_alloc);
    mu_run_test(test_ptr_dealloc);
    mu_run_test(test_ptr_index_operator);
    mu_run_test(test_ptr_for_loop);

    return nullptr;
  }
}
