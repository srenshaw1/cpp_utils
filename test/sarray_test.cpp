
/*
Author  : Seth Renshaw (srenshaw1@gmail.com)
Date    : 2017-08-27 14:50:01
Copyright: Seth Renshaw 2017-08-27 14:50:01
*/

#include "../helper/minunit.h"

#include "../sarray.hpp"

namespace test_SArray {

  char *test_all() {
    SArray<int32_t, 32> sarray;

    sarray(3, -1);
    sarray(7, -2);
    sarray(8, -3);
    sarray(3, -4);

    mu_assert(*sarray(0) ==  0, "wrong index instantiated");
    mu_assert(*sarray(1) ==  0, "wrong index instantiated");
    mu_assert(*sarray(2) ==  0, "wrong index instantiated");
    mu_assert(*sarray(3) == -4, "items not replaced");
    mu_assert(*sarray(4) ==  0, "wrong index instantiated");
    mu_assert(*sarray(5) ==  0, "wrong index instantiated");
    mu_assert(*sarray(6) ==  0, "wrong index instantiated");
    mu_assert(*sarray(7) == -2, "items not added");
    mu_assert(*sarray(8) == -3, "items not added");

    mu_assert(sarray.size == 3, "incorrect size after setting variables");

    return nullptr;
  }
}
