#if TEST_MODE

#include "../helper/minunit.h"

#include "../container_util.hpp"

#include "list_test.cpp"
#include "darray_test.cpp"
#include "farray_test.cpp"
#include "fsallocator_test.cpp"
#include "sarray_test.cpp"

char *test_collections()
{
  mu_suite_start();

  mu_run_test(test_List::test_all);
  mu_run_test(test_DArray::test_all);
  mu_run_test(test_FArray::test_all);
  mu_run_test(test_FSAllocator::test_all);
  mu_run_test(test_SArray::test_all);

  return nullptr;
}

RUN_TESTS(test_collections);

#endif