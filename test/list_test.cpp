
/*
Author  : Seth Renshaw (srenshaw1@gmail.com)
Date    : 2017-08-05 18:48:01
Copyright: Seth Renshaw 2017-08-05 18:48:01
*/

#include "../helper/minunit.h"

#include "../list.hpp"

namespace test_List {
  s32 nums [] = { 1, 2, 3 };
  List<s32> list;
  List<s32 *> ptr_list;

  char *test_push_back() {
    list.push_back(1);
    list.push_back(2);
    list.push_back(3);

    List<s32>::Node *node = list.first;
    mu_assert(node->data == 1, "Node data is not 1."); node = node->next;
    mu_assert(node->data == 2, "Node data is not 2."); node = node->next;
    mu_assert(node->data == 3, "Node data is not 3."); node = node->next;

    mu_assert(list.count == 3, "list.count is not 3.");

    return nullptr;
  }

  char *test_ptr_push_back() {
    ptr_list.push_back(&nums[0]);
    ptr_list.push_back(&nums[1]);
    ptr_list.push_back(&nums[2]);

    List<s32 *>::Node *node = ptr_list.first;
    mu_assert(*node->data == 1, "Node data is not 1."); node = node->next;
    mu_assert(*node->data == 2, "Node data is not 2."); node = node->next;
    mu_assert(*node->data == 3, "Node data is not 3.");

    mu_assert(ptr_list.count == 3, "ptr_list.count is not 3.");

    return nullptr;
  }

  char *test_pop_back() {
    mu_assert(list.pop_back() == 3, "Returned value is not 3.");
    mu_assert(list.pop_back() == 2, "Returned value is not 2.");
    mu_assert(list.pop_back() == 1, "Returned value is not 1.");
    mu_assert(list.pop_back() == 0, "Returned value is not default value.");

    mu_assert(list.count == 0, "list.count is not 0.");

    return nullptr;
  }

  char *test_ptr_pop_back() {
    mu_assert(*ptr_list.pop_back() == 3, "Returned value is not 3.");
    mu_assert(*ptr_list.pop_back() == 2, "Returned value is not 2.");
    mu_assert(*ptr_list.pop_back() == 1, "Returned value is not 1.");
    mu_assert(ptr_list.pop_back() == nullptr, "Returned value is not default pointer.");

    mu_assert(ptr_list.count == 0, "ptr_list.count is not 0.");

    return nullptr;
  }

  char *test_push_front() {
    list.push_front(1);
    list.push_front(2);
    list.push_front(3);

    List<s32>::Node *node = list.first;
    mu_assert(node->data == 3, "Node data is not 3."); node = node->next;
    mu_assert(node->data == 2, "Node data is not 2."); node = node->next;
    mu_assert(node->data == 1, "Node data is not 1.");

    mu_assert(list.count == 3, "list.count is not 3.");

    return nullptr;
  }

  char *test_ptr_push_front() {
    ptr_list.push_front(&nums[0]);
    ptr_list.push_front(&nums[1]);
    ptr_list.push_front(&nums[2]);

    List<s32 *>::Node *node = ptr_list.first;
    mu_assert(*node->data == 3, "Node data is not 3."); node = node->next;
    mu_assert(*node->data == 2, "Node data is not 2."); node = node->next;
    mu_assert(*node->data == 1, "Node data is not 1.");

    mu_assert(ptr_list.count == 3, "ptr_list.count is not 3.");

    return nullptr;
  }

  char *test_pop_front() {
    mu_assert(list.pop_front() == 3, "Returned value is not 3.");
    mu_assert(list.pop_front() == 2, "Returned value is not 2.");
    mu_assert(list.pop_front() == 1, "Returned value is not 1.");
    mu_assert(list.pop_front() == 0, "Returned value is not default value.");

    mu_assert(list.count == 0, "list.count is not 0.");

    return nullptr;
  }

  char *test_ptr_pop_front() {
    mu_assert(*ptr_list.pop_front() == 3, "Returned value is not 3.");
    mu_assert(*ptr_list.pop_front() == 2, "Returned value is not 2.");
    mu_assert(*ptr_list.pop_front() == 1, "Returned value is not 1.");
    mu_assert(ptr_list.pop_front() == nullptr, "Returned value is not default value.");

    mu_assert(ptr_list.count == 0, "ptr_list.count is not 0.");

    return nullptr;
  }

  char *test_for_loop() {
    list.push_back(1);
    list.push_back(2);
    list.push_back(3);

    u32 idx = 0;
    ForEach (list) {
      mu_assert(*it == nums[idx++], "Returned value is not correct.");
    }

    return nullptr;
  }

  char *test_ptr_for_loop() {
    ptr_list.push_back(&nums[0]);
    ptr_list.push_back(&nums[1]);
    ptr_list.push_back(&nums[2]);

    u32 idx = 0;
    ForEach (ptr_list)
      mu_assert(**it == nums[idx++], "Returned value is not correct.");

    return nullptr;
  }

  char *test_all() {
    mu_suite_start();

    mu_run_test(test_push_back);
    mu_run_test(test_pop_back);
    mu_run_test(test_push_front);
    mu_run_test(test_pop_front);
    mu_run_test(test_for_loop);

    mu_run_test(test_ptr_push_back);
    mu_run_test(test_ptr_pop_back);
    mu_run_test(test_ptr_push_front);
    mu_run_test(test_ptr_pop_front);
    mu_run_test(test_ptr_for_loop);

    return nullptr;
  }
}
