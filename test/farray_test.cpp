
/*
Author  : Seth Renshaw (srenshaw1@gmail.com)
Date    : 2017-08-05 18:13:11
Copyright: Seth Renshaw 2017-08-05 18:13:11
*/

#include "../helper/minunit.h"

#include "../farray.hpp"

namespace test_FArray {
  s32 nums [] = { 1, 2, 3 };
  FArray<s32,3> array;
  FArray<s32 *,3> ptr_array;

  char *test_push_back() {
    array.push_back(1);
    array.push_back(2);
    array.push_back(3);

    mu_assert(array.contents[0] == 1, "array data is not 1.");
    mu_assert(array.contents[1] == 2, "array data is not 2.");
    mu_assert(array.contents[2] == 3, "array data is not 3.");

    mu_assert(array.size == 3, "array size is not 3.");

    return nullptr;
  }

  char *test_ptr_push_back() {
    ptr_array.push_back(&nums[0]);
    ptr_array.push_back(&nums[1]);
    ptr_array.push_back(&nums[2]);

    mu_assert(ptr_array.contents[0] == &nums[0], "ptr_array data is not 1.");
    mu_assert(ptr_array.contents[1] == &nums[1], "ptr_array data is not 2.");
    mu_assert(ptr_array.contents[2] == &nums[2], "ptr_array data is not 3.");

    mu_assert(ptr_array.size == 3, "ptr_array size is not 3.");

    return nullptr;
  }

  char *test_pop_back() {
    mu_assert(array.pop_back() == 3, "returned value is not 3.");
    mu_assert(array.pop_back() == 2, "returned value is not 2.");
    mu_assert(array.pop_back() == 1, "returned value is not 1.");

    mu_assert(array.size == 0, "array size is not 0.");

    return nullptr;
  }

  char *test_ptr_pop_back() {
    mu_assert(ptr_array.pop_back() == &nums[2], "returned value is not 3.");
    mu_assert(ptr_array.pop_back() == &nums[1], "returned value is not 2.");
    mu_assert(ptr_array.pop_back() == &nums[0], "returned value is not 1.");
    mu_assert(ptr_array.pop_back() == nullptr, "returned value is not NULL.");

    mu_assert(ptr_array.size == 0, "ptr_array size is not 0.");

    return nullptr;
  }

  char *test_set() {
    array(0, 1);
    array(1, 2);
    array(2, 3);

    mu_assert(array.contents[0] == 1, "array data is not 1.");
    mu_assert(array.contents[1] == 2, "array data is not 2.");
    mu_assert(array.contents[2] == 3, "array data is not 3.");

    mu_assert(array.size == 3, "array size is not 3.");

    return nullptr;
  }

  char *test_ptr_set() {
    ptr_array(0, &nums[0]);
    ptr_array(1, &nums[1]);
    ptr_array(2, &nums[2]);

    mu_assert(ptr_array.contents[0] == &nums[0], "ptr_array data is not 1.");
    mu_assert(ptr_array.contents[1] == &nums[1], "ptr_array data is not 2.");
    mu_assert(ptr_array.contents[2] == &nums[2], "ptr_array data is not 3.");

    mu_assert(ptr_array.size == 3, "ptr_array size is not 3.");

    return nullptr;
  }

  char *test_get() {
    mu_assert(array(0) == 1, "array data is not 1.");
    mu_assert(array(1) == 2, "array data is not 2.");
    mu_assert(array(2) == 3, "array data is not 3.");

    return nullptr;
  }

  char *test_ptr_get() {
    mu_assert(ptr_array(0) == &nums[0], "array data is not 1.");
    mu_assert(ptr_array(1) == &nums[1], "array data is not 2.");
    mu_assert(ptr_array(2) == &nums[2], "array data is not 3.");

    return nullptr;
  }

  char *test_for_loop() {
    u32 idx = 0;
    ForEach (array)
      mu_assert(*it == nums[idx++], "iterator value is not correct.");

    return nullptr;
  }

  char *test_ptr_for_loop() {
    u32 idx = 0;
    ForEach (ptr_array)
      mu_assert(**it == nums[idx++], "iterator value is not correct.");

    return nullptr;
  }

  char *test_all() {
    mu_suite_start();

    mu_run_test(test_push_back);
    mu_run_test(test_pop_back);
    mu_run_test(test_set);
    mu_run_test(test_get);
    mu_run_test(test_for_loop);

    mu_run_test(test_ptr_push_back);
    mu_run_test(test_ptr_pop_back);
    mu_run_test(test_ptr_set);
    mu_run_test(test_ptr_get);
    mu_run_test(test_ptr_for_loop);

    return nullptr;
  }
}
