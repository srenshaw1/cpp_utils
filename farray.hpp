/*
Author: Seth Renshaw
Date Created: 8/10/2016
*/

#pragma once

#include "helper/template.hpp"
#include "types.h"

template<typename Type, uint32_t N>
struct FArray {
  struct Iterator {
    Iterator(FArray *fa): array(fa), index(0) {}

    bool operator!(void) { return index < array->size; }
    void operator++(void) { ++index; }
    Type *operator&(void) { return &array->contents[index]; }
    Type &operator*(void) { return array->contents[index]; }
    Type *operator->(void) { return &array->contents[index]; }

    uint32_t index;
    FArray *array;
  };

  bool push_back(Type item) {
    if (size + 1 <= N) {
      contents[size++] = item;
      return true;
    } else {
      return false;
    }
  }

  Type pop_back() {
    return size ? contents[--size] : Type();
  }

  inline
  void set(uint32_t i, Type el) {
    if (i >= N)
      return;
    if (i >= size)
      size = i+1;
    contents[i] = el;
  }

  inline
  Type get(uint32_t i) {
    return contents[i];
  }

  void clear() { size = 0; }

  void  operator()(uint32_t i, Type el) { set(i, el); }
  Type  operator()(uint32_t i) { return get(i); }
  Type &operator[](uint32_t i) { return contents[i]; }

  Iterator iterate() { return Iterator(this); }

  uint32_t size = 0;
  Type contents [N];
};
