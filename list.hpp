#pragma once

#include <stdlib.h>

#include "helper/dbg.h"
#include "types.h"
#include "container_util.hpp"

template <typename Data>
struct List {
  struct Node {
    Node(Data item): next(nullptr), prev(nullptr), data(item) {}

    Node *next;
    Node *prev;
    Data  data;
  };

  struct Iterator {
    Iterator(Node *start): current(start), index(0) {}

    b32   operator! () { return current != nullptr; }
    void  operator++() { current = current->next; }
    Data &operator* () { return current->data; }
    Data *operator->() { return &current->data; }

    Node *current;
    u32   index;
  };

  List (void): first(nullptr), last(nullptr), count(0) {}
  ~List(void);

  Data pop_back  (void) { return last  ? remove(this, last)  : Data(); }
  Data pop_front (void) { return first ? remove(this, first) : Data(); }
  void push_back (Data data);
  void push_front(Data data);
  void clear     (void);

  Iterator iterate() { return Iterator(first); }

  Node *first;
  Node *last;
  u32   count;
};

template <typename D>
List<D>::~List() {
  clear();
  delete last;
}

template <typename D>
void
List<D>::push_back(D data) {
  Node *node = new Node(data);
  if (!last) {
    first = node;
    last = node;
  } else {
    last->next = node;
    node->prev = last;
    last = node;
  }
  count++;
}

template <typename D>
void
List<D>::push_front(D data) {
  Node *node = new Node(data);
  if (!first) {
    first = node;
    last = node;
  } else {
    first->prev = node;
    node->next = first;
    first = node;
  }
  count++;
}

template <typename D>
void List<D>::clear() {
  for (Node *node = first; node; node = node->next)
    if (node->prev)
      delete node->prev;
}

//NOTE(seth): These are illegal for now
template <typename D> struct List<D &>;

//NOTE(seth): 'Private' function
template <typename D>
D
remove(List<D> *list, typename List<D>::Node *node) {
  check(list->first && list->last, "List is empty.");
  check(node, "node can't be NULL.");

  if (node == list->first && node == list->last) {
    list->first = nullptr;
    list->last = nullptr;
  } else if (node == list->first) {
    list->first = node->next;
    list->first->prev = nullptr;
  } else if (node == list->last) {
    list->last = node->prev;
    list->last->next = nullptr;
  } else {
    auto *after = node->next;
    auto *before = node->prev;
    after->prev = before;
    before->next = after;
  }

  list->count--;
  D result(node->data);
  delete node;
  node = nullptr;

error:
  return result;
}
