/*
Author: Seth Renshaw (srenshaw1@gmail.com)
Date Created: 2017-01-29
*/

template <typename Data>
struct Ptr
{
  Ptr(Data *d): data(d) {}

  inline Data *operator->(void) { return data; }
  inline Data &operator* (void) { return *data; }

  Data *data;
};

template <typename Data> struct Ptr<Data *>;
template <typename Data> struct Ptr<Data &>;

template <typename Data>
struct AutoPtr : public Ptr<Data>
{
  AutoPtr (Data *d): Ptr(d) {}
  ~AutoPtr(void) { delete data; }

  inline Data *operator->(void) { return data; }
  inline Data &operator* (void) { return *data; }
};